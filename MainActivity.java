package com.sammymuriuki.assignment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    RadioGroup radiogroup;
    RadioButton celsius,fahren;
    EditText edittext,edittext1;
    Button convert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        radiogroup=(RadioGroup)findViewById(R.id.radiogroup);
        celsius=(RadioButton)findViewById(R.id.celsius);
        fahren=(RadioButton)findViewById(R.id.fahrenheight);
        edittext=(EditText)findViewById(R.id.temp);
        edittext1=(EditText)findViewById(R.id.temp1);
        convert=(Button)findViewById(R.id.convert);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double value = Double.parseDouble(edittext.getText().toString());
                int selectId = radiogroup.getCheckedRadioButtonId();
                if (selectId == celsius.getId()) {

                    edittext1.setText(converter(value) +" " + "Degrees Fahrenheight");
                }

                else
                {
                    edittext1.setText(convertion(value) + " " + " Degrees Celcius");
                }
            }
        });

    }
    public double converter(double cel){
        Double outputt=((cel*9)/5)+32;
        return outputt;
    }
 public double convertion(double fah){
        double output1=((fah-32)*5/9);
        return output1;
    }
}


